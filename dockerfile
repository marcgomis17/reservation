# Node server
FROM node:16.17.0-alpine as node
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
RUN npm install -g json-server
COPY . .
COPY db.json .
RUN npm run build

FROM nginx:latest
COPY --from=node /app/dist/reservation/ /usr/share/nginx/html
#CMD ["nginx", "-g", "daemon off;", "json-server","--watch", "db.json","--port", "3000"]
CMD ["nginx", "-g", "daemon off;"]
