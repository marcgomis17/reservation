import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', './header-mobile.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void { }

  toggleMenu() {
    let menu = document.querySelector('#mobile-menu');
    if (menu?.classList.contains('hidden')) {
      menu.classList.replace('hidden', 'show');
    } else {
      menu?.classList.replace('show', 'hidden');
    }
  }

  navigateToStores() {
    this.router.navigateByUrl('/stores');
  }
}
