import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StoreComponent } from './store/store.component';

@NgModule({
  declarations: [
    HeaderComponent,
    StoreComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule,
    HeaderComponent,
    StoreComponent
  ]
})
export class AppSharedModule { }
