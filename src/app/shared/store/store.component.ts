import { Component, Input, OnInit } from '@angular/core';
import { Store } from 'src/app/model/store';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  @Input() store: Store | null = null;

  constructor() { }

  ngOnInit(): void { }
}
