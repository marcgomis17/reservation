export interface Store {
  id?: number;
  store_number: string;
  status: string;
  available: boolean;
}
