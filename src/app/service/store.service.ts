import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from '../config/constants';

import { Store } from '../model/store';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private storeUrl = `${baseUrl}stores`;

  constructor(private http: HttpClient) { }

  getStores(): Observable<Store[]> {
    return this.http.get<Store[]>(this.storeUrl);
  }

  getStore(id: number): Observable<Store> {
    return this.http.get<Store>(`${this.storeUrl}/${id}`);
  }
}
