import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BoxChoiceComponent } from "./box-choice.component";
import { StoreDetailsComponent } from "./components/store-details/store-details.component";

const routes: Routes = [
  {
    path: '',
    component: BoxChoiceComponent,
    children: [
      { path: 'details/:id', component: StoreDetailsComponent }
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class BoxChoiceRoutingModule { }
