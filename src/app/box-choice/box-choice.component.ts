import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Store } from "../model/store";
import { StoreService } from "../service/store.service";

@Component({
  selector: 'app-box-choice',
  templateUrl: 'box-choice.component.html',
  styleUrls: ['./box-choice.component.scss', './box-choice-mobile.component.scss'],
})

export class BoxChoiceComponent implements OnInit {
  storeList: Observable<Store[]> | undefined = undefined;

  constructor(private storeService: StoreService) { }

  ngOnInit(): void {
    this.storeList = this.storeService.getStores();
  }
}
