import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxChoiceRoutingModule } from './box-choice-routing.module';
import { AppSharedModule } from '../shared/shared.module';
import { BoxChoiceComponent } from './box-choice.component';
import { StoresMapComponent } from './components/stores-map/stores-map.component';
import { StoreDetailsComponent } from './components/store-details/store-details.component';

@NgModule({
  declarations: [
    BoxChoiceComponent,
    StoresMapComponent,
    StoreDetailsComponent
  ],
  imports: [
    CommonModule,
    BoxChoiceRoutingModule,
    AppSharedModule
  ]
})
export class BoxChoiceModule { }
