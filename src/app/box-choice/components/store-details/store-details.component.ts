import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Store } from 'src/app/model/store';
import { StoreService } from 'src/app/service/store.service';

@Component({
  selector: 'app-store-details',
  templateUrl: './store-details.component.html',
  styleUrls: ['./store-details.component.scss']
})
export class StoreDetailsComponent implements OnInit {
  detailsPage: any;
  store$: Observable<Store> | undefined;
  storeId: number = 0;

  constructor(private route: ActivatedRoute, private router: Router, private storeService: StoreService) { }

  ngOnInit(): void {
    this.detailsPage = document.getElementById('details-page');
    this.route.params.pipe(
      map(res => {
        this.storeId = res['id'];
        if (res['id'] !== undefined || res['id'] !== null) {
          if (this.detailsPage?.classList.contains('hidden')) {
            this.detailsPage?.classList.replace('hidden', 'opened');
          }
        }
      })
    ).subscribe();
    this.store$ = this.storeService.getStore(this.storeId);
  }

  closeDetails() {
    let detailsPage = document.getElementById('details-page');
    detailsPage?.classList.replace('opened', 'closed');
    setTimeout(() => {
      this.router.navigateByUrl('/stores');
    }, 500);
  }
}
