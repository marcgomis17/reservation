import { Component, Input, OnInit } from '@angular/core';
import { Store } from 'src/app/model/store';

@Component({
  selector: 'app-stores-map',
  templateUrl: './stores-map.component.html',
  styleUrls: ['./stores-map.component.scss']
})
export class StoresMapComponent implements OnInit {
  @Input() stores: Store[] = [];

  constructor() { }

  ngOnInit(): void { }

  openDetails() {
    let detailsPage = document.getElementById('details-page');
    if (detailsPage?.classList.contains('hidden')) {
      detailsPage?.classList.replace('hidden', 'opened');
    } if (detailsPage?.classList.contains('closed')) {
      detailsPage?.classList.replace('closed', 'opened');
    }
  }
}
