import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoresMapComponent } from './stores-map.component';

describe('StoresMapComponent', () => {
  let component: StoresMapComponent;
  let fixture: ComponentFixture<StoresMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoresMapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StoresMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
