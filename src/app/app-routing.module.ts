import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: 'stores', pathMatch: 'full' },
  // { path: 'home', loadChildren: ()=>import('./home/home.module').then(m=>module.HomeModule)},
  { path: '', redirectTo: 'stores', pathMatch: 'full' },
  { path: 'stores', loadChildren: () => import('./box-choice/box-choice.module').then(m => m.BoxChoiceModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
